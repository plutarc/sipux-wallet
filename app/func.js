(function ($) {
    $.fn.txTable = function (walletAddr, apiAddr, txDirection, txDate, txHash, txValue) {
        $(this)
            .append($('<div>')
                .addClass('row bg-light mx-1 my-2 p-1 txTable-row')
                .append($('<div>')
                    .addClass('col-1')
                    .append($('<span>')
                        .addClass(function () {
                            if (txDirection) {
                                return "oi oi-arrow-thick-left text-danger"
                            } else {
                                return "oi oi-arrow-thick-right text-success"
                            }
                        })
                    )
                )
                .append($('<div>')
                    .addClass('col-2 text-right text-monospace')
                    .append($('<small>')
                        .text(txDate)
                    )
                )
                .append($('<div>')
                    .addClass('col-7 text-monospace')
                    .append($('<a>')
                        .text(txHash)
                        .attr('href', apiAddr + txHash)
                        .attr('target', '_blank')
                    )
                )
                .append($('<div>')
                    .addClass('col-2 text-right text-monospace font-weight-bold')
                    .addClass(function () {
                        if (txDirection) {
                            return "text-danger"
                        } else {
                            return "text-success"
                        }
                    })
                    .text(function () {
                        var s
                        if (txDirection) {
                            s = "-"
                        } else {
                            s = "+"
                        }
                        return s + (txValue).toFixed(8)
                    })
                )
                .append($('<div>')
                    .addClass('col-12 txTable-details')
                    .hide()
                    .text('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elementum justo enim, vel interdum ligula aliquam ut. Maecenas faucibus dictum semper. Praesent accumsan erat erat, vitae iaculis sem consequat ultricies.')
                )
            )
    }
})(jQuery);

function timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours() < 10 ? '0' + a.getHours() : a.getHours();
    var min = a.getMinutes() < 10 ? '0' + a.getMinutes() : a.getMinutes();
    var sec = a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
}